from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    def test_solve1(self):
        r = "A Madrid Move London\nB London Move Austin\nC Austin Move Madrid"
        self.assertEqual(
            diplomacy_solve(r), "A London\nB Austin\nC Madrid\n")
            
    def test_solve2(self):
        r = "A Madrid Hold\nB London Support A\nC Austin Move Madrid\nD Dallas Support C"
        self.assertEqual(
            diplomacy_solve(r), "A [DEAD]\nB [DEAD]\nC [DEAD]\nD [DEAD]\n")
            
    def test_solve3(self):
        r = "A Madrid Hold\nB London Hold\nC Austin Hold"
        self.assertEqual(
            diplomacy_solve(r), "A Madrid\nB London\nC Austin\n")




# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
