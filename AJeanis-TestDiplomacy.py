from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):

    def test_solve_1(self):
        r = StringIO("A Lubbock Hold\nB Austin Move Lubbock\nC Dallas Support B\nD Houston Support A\nE Eastland Support C\nF Freeland Support E\nG Glock Move Freeland")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(),'A [Dead]\nB [Dead]\nC [Dead]\nD [Dead]\nE Eastland\nF [Dead]\nG [Dead]')

    
    def test_solve_2(self):
        r = StringIO('A Lubbock Hold\nB Austin Move Lubbock\nC Dallas Support B\nE Eastland Support A\nF Freeland Move Lubbock')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Lubbock\nB [Dead]\nC Dallas\nE Eastland\nF [Dead]')
    
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [Dead]\nB [Dead]\nC [Dead]\nD [Dead]')


if __name__ == "__main__":
    main()

