import Diplomacy
from io import StringIO
from unittest import main, TestCase

"""
Unit tests for diplomacy_solve()
"""


class TestDiplomacy (TestCase):
    def test_solve1(self):
        r = StringIO('A Madrid Support B\nB Austin Hold\nC Houston Move Austin')
        w = StringIO()
        Diplomacy.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB Austin\nC [dead]\n')


    def test_solve3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        Diplomacy.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')



    def test_solve2(self):
        r = StringIO('A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E')
        w = StringIO()
        Diplomacy.diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n')

    def test_read1(self):
        r = 'C London Support B'
        output = Diplomacy.diplomacy_read(r)
        self.assertEqual(output, ['C', 'London', 'Support', 'B'])

    def test_read2(self):
        r = 'C London Hold'
        output = Diplomacy.diplomacy_read(r)
        self.assertEqual(output, ['C', 'London', 'Hold', 'Start'])

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
