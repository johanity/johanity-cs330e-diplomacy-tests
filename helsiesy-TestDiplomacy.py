from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDoplomacy (TestCase):
    
    # evaluate
    def test_eval_1(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        sol = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'}

        army = diplomacy_eval(moves)
        self.assertEqual(len(army), len(sol))

        for i in sol:
            self.assertEqual(sol[i], army[i])

    def test_eval_2(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        sol = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'}

        army = diplomacy_eval(moves)
        self.assertEqual(len(army), len(sol))

        for i in sol:
            self.assertEqual(sol[i], army[i])

    def test_eval_3(self):
        moves = StringIO('A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Move London')
        sol = {'A': 'Barcelona', 'B': 'Madrid', 'C': 'Austin', 'D': 'London'}

        army = diplomacy_eval(moves)
        self.assertEqual(len(army), len(sol))

        for i in sol:
            self.assertEqual(sol[i], army[i])
    
    # printing
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'madrid', 'B': 'Dallas', 'C': 'Houston', 'D': 'LA', 'E': 'New York'})
        self.assertEqual(w.getvalue(), "A madrid\nB Dallas\nC Houston\nD LA\nE New York\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]'})
        self.assertEqual(w.getvalue(), "A [dead]\n")
        
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'Russia', 'B': 'China'})
        self.assertEqual(w.getvalue(), "A Russia\nB China\n")

    # solve
    def test_solve_1(self):
        moves = StringIO('A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E')
        w = StringIO()
        diplomacy_solve(moves, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n")

    def test_solve_2(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona')
        w = StringIO()
        diplomacy_solve(moves, w)
        # print(w.getvalue())
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC Barcelona\n')


    def test_solve_3(self):
        moves = StringIO('A Madrid Hold\nB Barcelona Support A\nC London Move Madrid\nD Dallas Move Barcelona')
        w = StringIO()
        diplomacy_solve(moves, w)
        # print(w.getvalue())
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    
    
# 
if __name__ == "__main__":
    main()

