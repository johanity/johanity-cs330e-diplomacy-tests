#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "Z Narnia Hold\n"
        self.assertEqual(diplomacy_read(s), {"Z": {"location": "Narnia", "action": ["Hold"]}})

    def test_read_2(self):
        s = "A Narnia Hold\nB Wardrobe Move Narnia\nC Home Support A\nD Grave Support B\n"
        self.assertEqual(diplomacy_read(s), {
            "A": {
                "location": "Narnia",
                "action": ["Hold"]
            },
            "B": {
                "location": "Wardrobe",
                "action": ["Move", "Narnia"]
            },
            "C": {
                "location": "Home",
                "action": ["Support", "A"]
            },
            "D": {
                "location": "Grave",
                "action": ["Support", "B"]
            }
        })

    def test_read_3(self):
        s = "A Narnia Hold\nB Wardrobe Move Narnia\nC Home Support A\n"
        self.assertEqual(diplomacy_read(s), {"A": {"location": "Narnia", "action": ["Hold"]}, "B": {"location": "Wardrobe", "action": ["Move", "Narnia"]}, "C": {"location": "Home", "action": ["Support", "A"]}})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"Z": "Narnia"})
        self.assertEqual(w.getvalue(), "Z Narnia\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "[dead]", "C": "Home", "D": "Grave"})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Home\nD Grave\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": "Narnia", "B": "[dead]", "C": "Home"})
        self.assertEqual(w.getvalue(), "A Narnia\nB [dead]\nC Home\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Narnia Support B\nB Wardrobe Support E\nC Home Move School\nD Grave Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Narnia\nB Wardrobe\nC School\nD Grave\n")

    def test_solve_2(self):
        r = StringIO("A Narnia Hold\nB Wardrobe Move Narnia\nC Home Support A\nD Grave Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Home\nD Grave\n")

    def test_solve_3(self):
        r = StringIO("A Narnia Move Wardrobe\nB Wardrobe Support C\nC Home Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Wardrobe\nB [dead]\nC Home\n")

    def test_solve_4(self):
        r = StringIO("A Narnia Support B\nB Wardrobe Support C\nC Home Hold\nD Grave Move Wardrobe\nE School Move Home\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Narnia\nB Wardrobe\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Narnia Support B\nB Wardrobe Support C\nC Home Hold\nD Grave Move Narnia\nE School Move Home\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Wardrobe\nC Home\nD [dead]\nE [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Narnia Move Wardrobe\nB Wardrobe Move Narnia\nC Home Move Grave\nD Grave Move Home\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Wardrobe\nB Narnia\nC Grave\nD Home\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
............
----------------------------------------------------------------------
Ran 12 tests in 0.002s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
...........
----------------------------------------------------------------------
Ran 12 tests in 0.002s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          65      0     44      0   100%
TestDiplomacy.py      51      0      2      0   100%
--------------------------------------------------------------
TOTAL                116      3     46      3   100%
"""
