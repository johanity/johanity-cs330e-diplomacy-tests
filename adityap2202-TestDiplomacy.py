from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, diplomacy_eval


class TestDiplomacy(TestCase):

    def test_solve0(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n D Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Paris\n")

    def test_solve1(self):
        r = StringIO(
            "A Madrid Support B\nB Barcelona Move Paris\nC London Move Barcelona\n D Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Paris\nC Barcelona\nD [dead]\n")

    def test_solve2(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Hold\nC London Support A\n D Paris Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve3(self):
        r = StringIO(
            "A Madrid Support B\nB Austin Hold\nC Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\n")

    def test_solve4(self):
        r = StringIO(
            "A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n")

    def test_solve5(self):
        r = StringIO(
            "A Austin Move Barcelona\nB Barcelona Support C \nC London Move Paris\nD Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


if __name__ == "__main__": #pragma: no cover
    main()


