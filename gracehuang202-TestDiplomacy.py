
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print, Army

class TestDiplomacy (TestCase):
    
    def test_read_1(self):
        r = StringIO("A Rome Hold")
        list = diplomacy_read(r)
        ans = []
        ans.append(Army("A","Rome","Hold"))
        self.assertEqual(list,  ans)
    def test_read_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        list = diplomacy_read(r)
        ans = []
        ans.append(Army("A","Madrid","Hold"))
        ans.append(Army("B", "Barcelona", "Move", "Madrid"))
        ans.append(Army("C", "London", "Support", "B"))
        self.assertEqual(list,  ans)

    def test_read_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        list = diplomacy_read(r)
        ans = []
        ans.append(Army("A","Madrid","Hold"))
        ans.append(Army("B", "Barcelona", "Move", "Madrid"))
        self.assertEqual(list,  ans)

    def test_solve_1(self):
        armies = [Army('A', 'Madrid', 'Move', 'Denver'),
                    Army('B', 'Denver', 'Move', 'Madrid')]
        v = diplomacy_solve(armies)
        self.assertEqual(v[0].status, 'Madrid')
        self.assertEqual(v[1].status, 'Denver')

    def test_solve_2(self):
        armies = [Army('A', 'Madrid', 'Move', 'SanFran'),
                    Army('B', 'Denver', 'Move', 'Madrid'),
                    Army('C', 'Denver', 'Move', 'Madrid'),
                    Army('D', 'SanFran', 'Support', 'A')]
        v = diplomacy_solve(armies)
        
        self.assertEqual(v[0].status, '[dead]')
        self.assertEqual(v[1].status, '[dead]')
        self.assertEqual(v[2].status, '[dead]')
        self.assertEqual(v[3].status, '[dead]')

    def test_solve_3(self):
        armies = [Army('A', 'Madrid', 'Hold'),
                    Army('B', 'Denver', 'Move', 'Madrid'),
                    Army('C', 'Chicago', 'Move', 'Madrid'),
                    Army('D', 'SanFran', 'Support', 'A'),
                    Army('E', 'Phoneix', 'Support', 'A'),
                    Army('F', 'Boston', 'Support', 'SanFran')]
        v = diplomacy_solve(armies)
        self.assertEqual(v[0].status, 'Madrid')
        self.assertEqual(v[1].status, '[dead]')
        self.assertEqual(v[2].status, '[dead]')
        self.assertEqual(v[3].status, 'SanFran')
        self.assertEqual(v[4].status, 'Phoneix')
        self.assertEqual(v[5].status, 'Boston')
    
    def test_print_1(self):
        w = StringIO()
        armies = [Army('A', 'Madrid', 'Hold'),
                    Army('B', 'Barcelona', 'Hold')]
        diplomacy_print(armies,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")
        
    def test_print_2(self):
        w = StringIO()
        armies = [Army('B', 'Madrid', 'Hold'),
                    Army('A', 'Barcelona', 'Hold')]
        diplomacy_print(armies,w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")

    def test_print_3(self):
        w = StringIO()
        armies = [Army('C', 'SanFran', 'Hold'),
                  Army('A', 'Madrid', 'Hold'),
                  Army('D', 'Boston', 'Hold'),
                    Army('B', 'Barcelona', 'Hold')]
        diplomacy_print(armies,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC SanFran\nD Boston\n")


 


if __name__ == "__main__":
    main()



""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 
$ coverage report -m   >> TestDiplomacy.out

"""
