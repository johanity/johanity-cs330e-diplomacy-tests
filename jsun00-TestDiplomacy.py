#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase) :
    
    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Tokyo Hold\nE Lisbon Move Tokyo\nF Helsinki Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        expected_results = "A [dead]\nB Madrid\nC London\nD Tokyo\nE [dead]\nF Helsinki\n"
        self.assertEqual(w.getvalue(), expected_results)

    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Tokyo Move London\nE Lisbon Move London\nF Rio Hold\nG Denver Move Rio\nH Bogota Move Rio")
        w = StringIO()
        diplomacy_solve(r, w)
        expected_results = "A [dead]\nB [dead]\nC Barcelona\nD [dead]\nE [dead]\nF [dead]\nG [dead]\nH [dead]\n"
        self.assertEqual(w.getvalue(), expected_results)

    def test_diplomacy_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Tokyo Move Madrid\nF Lisbon Move London\nG Nairobi Support F\nH Denver Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        expected_results = "A [dead]\nB Madrid\nC [dead]\nD [dead]\nE [dead]\nF London\nG Nairobi\nH Denver\n"
        self.assertEqual(w.getvalue(), expected_results)

if __name__ == "__main__": #pragma: no cover
    main()
