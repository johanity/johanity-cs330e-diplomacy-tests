#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from tokenize import String
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Berlin Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Berlin Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Berlin Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Berlin\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Move Berlin\nB Berlin Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_5(self):
        r = StringIO("A Portland Support B\nB Plano Support C\nC Charlotte Hold\nD Omaha Move Plano\nE Sugarland Move Charlotte \nF TampaBay Support E")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Portland\nB Plano\nC [dead]\nD [dead]\nE Charlotte\nF TampaBay\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Support B\nB Berlin Move Plano\nC Plano Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Plano\nC [dead]\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Move London\nB Berlin Hold\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Berlin\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__": 
    main()

""" #pragma: no cover

"""